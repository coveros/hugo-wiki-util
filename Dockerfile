# Utility to run hugo server and npm commands for development
#
# build with `docker built -t wiki-util .`
# run with `docker run -v $(pwd):/src -p 1313:1313 wiki-util hugo server -D`
# view site on https://localhost:1313
# to run interactive:
#   docker run --rm -it -v $(pwd):/src -p 1313:1313 wiki-util bash

# alpine base image. We need the "extended" version of hugo
FROM klakegg/hugo:ext-alpine

# Install Git so we can fetch/manipluate artifacts in our repository
RUN apk update && apk upgrade && apk add git

# Install NodeJS and NPM so we can compile/build pages
RUN apk add --update nodejs npm

# Override the `hugo` default entrypoint from parent container
ENTRYPOINT []
