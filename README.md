# hugo-wiki-util

A simple utility for testing and building Hugo-based wiki sites

This simple docker container has Hugo and NodeJS installed in it such that
it can run a Hugo server, compile/build a nodeJS-based hugo site for hosting
on a generic HTTP server, and otherwise help debug your Hugo sites.

It was originally built for the Coveros solution wiki, but can easily be used
for other purposes.

# Building the Container Image

Simple build:

```
docker build -t wiki-util .
```

# Using the container image

Various things you might want to do:
```
# Run Hugo server for local development and testing
docker run --rm -v $(pwd):/src -p 1313:1313 wiki-util hugo server -D

# Run NPM install to build/compile the site
docker run --rm -v $(pwd):/src wiki-util npm install

# Interactive shell into hugo container for debugging
docker run --rm -it -v $(pwd):/src -p 1313:1313 wiki-util bash
```
# TODO

* Describe Gitlab CI process
* Describe how to update/test